import S3 from './s3'
import PDF from './pdf'
import db from './database'
import uuidv4 from 'uuid/v4'

export default class Processor {
  constructor(sourcePath = null) {
    this._pdf = null
    this._sourceFile = null
    this._sourcePath = sourcePath
    this._pdfBucket = process.env.PDF_BUCKET
    this._pdfThumbsBucket = process.env.PDF_THUMBS_BUCKET
  }

  async run () {
    await this._loadPdf()
    await this._storeToDatabase()
    await this._setProcessingStatus('in progress')

    const thumbs = await this._pdf.makeThumbs()
    const thumbsWithLink = this._applyLinks(thumbs)
    
    await this._storeLinksToDatabase(thumbsWithLink)
    await this._uploadThumbs(thumbsWithLink)
    await this._removeSourceFile()
    await this._setProcessingStatus('finished')
  }

  async _loadPdf () {
    this._sourceFile = await S3.getObject({
      Bucket: this._pdfBucket,
      Key: this._sourcePath
    }).promise()

    this._pdf = new PDF(this._sourceFile.Body)
  }

  async _removeSourceFile () {
    await S3.deleteObject({
      Bucket: this._pdfBucket,
      Key: this._sourcePath
    }).promise()

    this._pdf = null
    this._sourceFile = null
  }

  async _uploadThumbs (thumbs) {
    return Promise.all(thumbs.map(async thumb => {
      return S3.upload({
        Bucket: this._pdfThumbsBucket,
        Key: thumb.link,
        Body: thumb.content
      }).promise()
    }))
  }

  _applyLinks (thumbs) {
    const uniqueKey = uuidv4()

    return thumbs.map(thumb => {
      thumb.link = `${uniqueKey}/${thumb.page}.png`

      return thumb
    })
  }

  async _storeToDatabase () {
    const pageCount = await this._pdf.getPageCount()

    await db.batchWriteItem({
      RequestItems: {
        'pdfs': [{
          PutRequest: {
            Item: {
              "FileName": {
                S: this._sourcePath
              }, 
              "FileSize": {
                S: this._sourceFile.ContentLength.toString()
              }, 
              "PageCount": {
                S: pageCount.toString()
              },
              "ProcessingStatus": {
                S: 'new'
              }
            }
          }
        }]
      }
    }).promise()
  }

  async _storeLinksToDatabase (thumbs) {
    await db.updateItem({
      Key: {
        'FileName': {
          S: this._sourcePath
        }
      },
      TableName: 'pdfs',
      AttributeUpdates: {
        'Thumbs': {
          Action: 'PUT',
          Value: {
            SS: thumbs.map(thumb => thumb.link)
          }
        }
      }
    }).promise()
  }

  async _setProcessingStatus (status) {
    await db.updateItem({
      Key: {
        'FileName': {
          S: this._sourcePath
        }
      },
      TableName: 'pdfs',
      AttributeUpdates: {
        'ProcessingStatus': {
          Action: 'PUT',
          Value: {
            S: status
          }
        }
      }
    }).promise()
  }
}