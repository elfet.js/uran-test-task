import { S3, Endpoint } from 'aws-sdk'

export default new S3({
  s3ForcePathStyle: true,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID, // This specific key is required when working offline
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  endpoint: new Endpoint(process.env.S3_HOST),
})