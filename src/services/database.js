import { DynamoDB } from 'aws-sdk'

const database = new DynamoDB({
  region: process.env.AWS_REGION,
  endpoint: process.env.DB_HOST,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

export default database