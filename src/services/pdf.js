import gm from 'gm'

const imageMagick = gm.subClass({ imageMagick: true })

export default class PDF {
  constructor (content = null) {
    this._pdf = imageMagick(content)

    return this
  }

  async getPageCount() {
    return new Promise((resolve, reject) => {
      this._pdf.identify((err, data) => {
        if (!err) {
          resolve(data.Scene.length)
        } else {
          reject(err)
        }
      })
    })
  }

  async convertFrame (frameIndex) {
    return new Promise((resolve, reject) => {
      this._pdf.quality(100).selectFrame(frameIndex).toBuffer('png', (err, buffer) => {
        if (!err) {
          resolve({ page: frameIndex + 1, content: buffer })
        } else {
          reject(err)
        }
      })
    })
  }

  async makeThumbs () {
    const pageCount = await this.getPageCount()

    return Promise.all(
      Array(pageCount).fill().map(
        async (f, i) => this.convertFrame(i)
      )
    )
  }
}