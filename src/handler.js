import Processor from './services/processor'

export const pdfProcessor = async (event, context, callback) => {
  if (!event.Records.length) {
    callback(null, 'No files.')
  } else {
    Promise.all(event.Records.map(async record => {
      if (record.eventName !== 'ObjectRemoved:Delete') {
        const processor = new Processor(record.s3.object.key)

        return processor.run()
      }
    }))
      .then(() => {
        callback(null, 'All files were processed.')
      })
      .catch(error => {
        callback(error, 'Files were processed with errors.')
      })
  }
};