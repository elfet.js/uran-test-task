# Uran Test Task

## Requirements

* Node.js 12.x
* imagemagick
* graphicsmagick
* serverless

### Additional Requirements

* AWS CLI

## Getting started
First download and install [GraphicsMagick](http://www.graphicsmagick.org/) or [ImageMagick](http://www.imagemagick.org/). In Mac OS X, you can simply use [Homebrew](http://mxcl.github.io/homebrew/) and do:

    brew install imagemagick
    brew install graphicsmagick

then either use npm:

    npm i -g serverless
    npm i

you need to install dynamodb localy

    sls dynamodb install

after that you should run the application

    sls offline start --stage local

## Testing

    aws --endpoint http://localhost:4569 s3api put-object --bucket uran-pdf-bucket --key pdf/test.pdf --body <path/to/your/pdf>